﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeManager : MonoBehaviour {

	private AudioSource mainAudioSource;

	[SerializeField]
	private AudioClip buttonSound;

	private void Start()
	{
		if(MainManager.INSTANCE != null)
		{
			mainAudioSource = MainManager.INSTANCE.gameObject.GetComponent<AudioSource>();
		}
		else
		{
			mainAudioSource = Camera.main.GetComponent<AudioSource>();
		}
	}

	public void loadScene(int sceneIndex)
	{
		StartCoroutine(loadSceneCoroutine(sceneIndex));
	}

	private IEnumerator loadSceneCoroutine(int sceneIndex)
	{
		Time.timeScale = 1f;
		if(MainManager.INSTANCE != null)
		{
			mainAudioSource.clip = MainManager.INSTANCE.sonDeBouton;
			mainAudioSource.Play();
		}
		else
		{
			mainAudioSource.clip = buttonSound;
			mainAudioSource.Play();
		}
		yield return new WaitForSeconds(0.5f);
		SceneManager.LoadScene(sceneIndex);
	}

	public void exitGame()
	{
		StartCoroutine(exitGameCoroutine());
	}

	private IEnumerator exitGameCoroutine()
	{
		if(MainManager.INSTANCE != null)
		{
			mainAudioSource.clip = MainManager.INSTANCE.sonDeBouton;
			mainAudioSource.Play();
		}
		else
		{
			mainAudioSource.clip = buttonSound;
			mainAudioSource.Play();
		}
		yield return new WaitForSeconds(0.5f);
		Application.Quit();
	}
}
