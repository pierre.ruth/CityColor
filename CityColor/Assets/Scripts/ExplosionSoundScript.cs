﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSoundScript : MonoBehaviour {

	// Use this for initialization
	private void Start () 
	{
		gameObject.GetComponent<AudioSource>().clip = MainManager.INSTANCE.sonDExplosion;
		gameObject.GetComponent<AudioSource>().Play();
	}
}
