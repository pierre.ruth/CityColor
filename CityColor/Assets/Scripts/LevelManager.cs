﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour 
{
	#region Variables

	///<summary>
	///	Cette variable permet de savoir si on doit faire défiler le niveau ou pas
	///</summary>
	[SerializeField]
	private bool isMoving = true;

	///<summary>
	///	Cette variable détermine la vitesse de défilement du niveau
	///</summary>
	[SerializeField]
	private float movementSpeed;

	///<summary>
	///	Cette variable contient l'objet contenant les buildings
	///</summary>
	[SerializeField]
	private Transform buildingContainer;

	///<summary>
	///	Cette variable contient l'objet contenant les parcs
	///</summary>
	[SerializeField]
	private Transform parkContainer;

	private Vector3 initialPosition;

	public bool isGoingBack = false;

	#endregion

	#region Functions

	private void Start()
	{
		initialPosition = transform.position;
	}

	///<summary>
	///	Cette fonction se déclenche à intervalles régulières
	///</summary>
	private void FixedUpdate()
	{
		if(isMoving)
		{
			transform.Translate(new Vector3(0f, 0f, -movementSpeed));
			if(isGoingBack && transform.position == initialPosition)
			{
				setIsMoving(false);
			}
		}
	}

	///<summary>
	///	Cette fonction détermine si le niveau scrolle
	///</summary>
	public void setIsMoving(bool moving)
	{
		isMoving = moving;
	}

	///<summary>
	///	Cette fonction retourne l'état de scroll du niveau
	///</summary>
	public bool getIsMoving()
	{
		return isMoving;
	}

	///<summary>
	///	Cette fonction détermine la vitesse de défilement du niveau
	///</summary>
	public void setMovingSpeed(float newSpeed)
	{
		movementSpeed = newSpeed;
	}

	///<summary>
	///	Cette fonction retourne la vitesse de défilement du niveau
	///</summary>
	public float getMovingSpeed()
	{
		return movementSpeed;
	}

	///<summary>
	///	Cette fonction se déclenche lors de la fin du niveau
	///</summary>
	public void endLevel()
	{
		StartCoroutine(endLevelCoroutine());
	}

	private IEnumerator endLevelCoroutine()
	{
		setIsMoving(false);
		isGoingBack = true;
		MainManager.INSTANCE.pauseManager.showEndScreen(true);
		yield return new WaitForSeconds(2f);
		MainManager.INSTANCE.playerManager.gameObject.SetActive(false);
		setMovingSpeed(-movementSpeed);
		setIsMoving(true);
	}

	public void setBuildingParameters(string buildingType, int health, int score)
	{
		foreach(Transform building in buildingContainer)
		{
			switch(buildingType)
			{
				case "normal":
					if(building.GetComponent<BuildingManager>() != null)
					{
						BuildingManager buildingManager = building.GetComponent<BuildingManager>();
						buildingManager.setHealth(health);
						buildingManager.setScore(score);
					}
					break;

				case "firing":
					if(building.GetComponent<BuildingFireManager>() != null)
					{
						BuildingManager buildingManager = building.GetComponent<BuildingManager>();
						buildingManager.setHealth(health);
						buildingManager.setScore(score);
					}
					break;

				default:
					Debug.LogWarning(buildingType + " not a valid type");
					break;
			}
		}
	}

	public void setParkParameters(int score)
	{
		foreach(Transform park in parkContainer)
		{
			if(park.GetComponent<ParkManager>() != null)
			{
				park.GetComponent<ParkManager>().setScore(score);
			}
		}
	}

	#endregion
}
