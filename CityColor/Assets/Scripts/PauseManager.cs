using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;

public class PauseManager : MonoBehaviour {

	#region Variables

	[SerializeField]
	private GameObject pauseCanvas;

	[SerializeField]
	private GameObject uiCanvas;

	[SerializeField]
	private GameObject endLevelCanvas;

	[SerializeField]
	private TextMeshProUGUI countdownText;

	private bool isPaused = false;
	private bool startupCoroutine = false;

	private AudioClip clickSound;
	private AudioSource mainAudioSource;

	#endregion

	#region Functions

	// Use this for initialization
	void Start () 
	{
		clickSound = MainManager.INSTANCE.sonDeBouton;
		mainAudioSource = MainManager.INSTANCE.gameObject.GetComponent<AudioSource>();
		pauseCanvas.SetActive(false);
		uiCanvas.SetActive(true);
		endLevelCanvas.SetActive(false);
		StartCoroutine(startCountdownCoroutine());
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Cancel") && !startupCoroutine)
		{
			if(!isPaused)
			{
				pauseGame(true);
			}
			else
			{
				pauseGame(false);
			}
		}
	}

	public void pauseGame(bool pause)
	{
		mainAudioSource.clip = clickSound;
		mainAudioSource.Play();
		isPaused = pause;
		pauseCanvas.SetActive(pause);
		uiCanvas.SetActive(!pause);
		if(pause)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	private IEnumerator startCountdownCoroutine()
	{
		mainAudioSource.clip = MainManager.INSTANCE.sonDeCompteARebours;

		startupCoroutine = true;
		MainManager.INSTANCE.levelManager.setIsMoving(false);
		MainManager.INSTANCE.playerManager.canMove = false;

		countdownText.gameObject.SetActive(true);
		int i = 3;
		while(i > 0)
		{
			countdownText.text = i.ToString();
			i--;
			mainAudioSource.Play();
			yield return new WaitForSeconds(1f);
		}

		countdownText.gameObject.SetActive(false);

		MainManager.INSTANCE.levelManager.setIsMoving(true);
		MainManager.INSTANCE.playerManager.canMove = true;
		startupCoroutine = false;

		mainAudioSource.clip = MainManager.INSTANCE.sonDeDepart;
		mainAudioSource.Play();
	}

	public void showEndScreen(bool showing)
	{
		endLevelCanvas.SetActive(showing);
	}

	#endregion
}
