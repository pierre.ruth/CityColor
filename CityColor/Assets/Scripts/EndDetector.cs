/********************************************
* Product:	CityColor
* Developer:	Pierre Ruth - pierre.ruth@larelevedufutur.org
* Company:	La Relève Du Futur
* Date:		21/07/2018 00:58:00
********************************************/

using UnityEngine;

public class EndDetector : MonoBehaviour 
{
	#region Variables

	private bool levelFinished = false;

	private AudioSource mainAudioSource;

	#endregion

	#region Functions

	private void Start()
	{
		mainAudioSource = MainManager.INSTANCE.GetComponent<AudioSource>();
	}

	private void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Player" && !MainManager.INSTANCE.levelManager.isGoingBack && !levelFinished)
		{
			levelFinished = true;
			mainAudioSource.clip = MainManager.INSTANCE.sonDeDepart;
			mainAudioSource.Play();
			MainManager.INSTANCE.levelManager.endLevel();
			MainManager.INSTANCE.playerManager.playerEndLevel();
		}
	}

	#endregion	
}