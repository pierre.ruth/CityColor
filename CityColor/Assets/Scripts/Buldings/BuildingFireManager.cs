/********************************************
* Product:	CityColor
* Developer:	Pierre Ruth - pierre.ruth@larelevedufutur.org
* Company:	La Relève Du Futur
* Date:		01/07/2018 18:54:48
********************************************/

using UnityEngine;
using System.Collections;

public class BuildingFireManager : MonoBehaviour 
{
	#region Variables

	[SerializeField]
	private GameObject explosion;

	[SerializeField]
	private GameObject enemyProjectile;

	[SerializeField]
	private Transform cannon;

	[SerializeField]
	private Transform cannonMouth;

	[SerializeField]
	private Animator cannonAnim;

	[SerializeField]
	private float aimingDistance;

	[SerializeField]
	private float shootCooldown;

	private Transform player;

	private bool canShoot = true;

	#endregion

	#region Functions

	private void Start()
	{
		player = MainManager.INSTANCE.playerManager.transform;
	}

	private void FixedUpdate()
	{
		if(Vector3.Distance(cannon.position, player.position) < aimingDistance && player.GetComponent<PlayerManager>().canMove && !gameObject.GetComponent<BuildingManager>().isColored())
		{
			cannon.LookAt(new Vector3(player.position.x, cannon.position.y, player.position.z));
			cannon.rotation *= Quaternion.Euler(0f, 90f, 0f);
			if(canShoot)
			{
				StartCoroutine(shootProjectileCoroutine());
			}
		}
	}

	private IEnumerator shootProjectileCoroutine()
	{
		//Animate ?
		Vector3 newPosition = new Vector3(cannonMouth.position.x, MainManager.INSTANCE.playerManager.transform.position.y, cannonMouth.position.z);
		Instantiate(enemyProjectile, newPosition, cannon.rotation, MainManager.INSTANCE.playerManager.transform.parent);
		canShoot = false;
		yield return new WaitForSeconds(shootCooldown);
		canShoot = true;
	}

	public void explodeTurret()
	{
		StartCoroutine(explodeTurretCoroutine());
	}

	private IEnumerator explodeTurretCoroutine()
	{
		GameObject newExplosion = Instantiate(explosion, cannonAnim.transform.position/*cannonAnim.transform.parent.InverseTransformPoint(cannon.position)*/, Quaternion.identity, cannonAnim.transform);
		cannonAnim.transform.GetChild(0).gameObject.SetActive(false);
		yield return new WaitForSeconds(2f);
		Destroy(newExplosion);
	}

	#endregion	
}
