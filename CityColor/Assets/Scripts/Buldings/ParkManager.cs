using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParkManager : MonoBehaviour {

	private Color originalGroundColor;

	[SerializeField]
	private Color barrenGroundColor = Color.white;

	[SerializeField]
	private float popInRate = 0.05f;

	public int scoreValue;

	private bool hasBloomed = false;

	// Use this for initialization
	void Start () 
	{
		originalGroundColor = gameObject.GetComponent<Renderer>().material.color;
		gameObject.GetComponent<AudioSource>().clip = MainManager.INSTANCE.sonDeParcFleuri;
		initializePark();
	}
	
	private void initializePark()
	{
		gameObject.GetComponent<Renderer>().material.color = barrenGroundColor;
		foreach(Transform element in transform)
		{
			element.gameObject.SetActive(false);
		}
	}

	private void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "FlowerBomb" && !hasBloomed)
		{
			bloomPark();
		}
	}

	[ContextMenu("Make it Bloom !")]
	public void bloomPark()
	{
		hasBloomed = true;
		MainManager.INSTANCE.scoreManager.addToScore(scoreValue);
		gameObject.GetComponent<Renderer>().material.color = originalGroundColor;
		StartCoroutine(animatePark());
	}

	private IEnumerator animatePark()
	{
		foreach(Transform element in transform)
		{
			StartCoroutine(animateParkElement(element, element.localScale));
			gameObject.GetComponent<AudioSource>().Play();
			yield return new WaitForSeconds(popInRate);
		}
	}

	private IEnumerator animateParkElement(Transform element, Vector3 initialScale)
	{
		element.localScale = new Vector3(0f, 0f, 0f);
		element.gameObject.SetActive(true);
		while(element.localScale.x < initialScale.x * 1.05f)
		{
			element.localScale = Vector3.Lerp(element.localScale, initialScale * 1.1f, 0.5f);
			yield return null;
		}

		while(element.localScale.x > initialScale.x * 0.9f)
		{
			element.localScale = Vector3.Lerp(element.localScale, initialScale * 0.85f, 0.2f);
			yield return null;
		}

		while(element.localScale.x < initialScale.x * 0.95f)
		{
			element.localScale = Vector3.Lerp(element.localScale, initialScale, 0.2f);
			yield return null;
		}
	}

	public void setScore(int newScore)
	{
		scoreValue = newScore;
	}
}
