/********************************************
* Product:	CityColor
* Developer:	Pierre Ruth - pierre.ruth@larelevedufutur.org
* Company:	La Relève Du Futur
* Date:		27/06/2018 12:26:36
********************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingManager : MonoBehaviour 
{
	#region Variables

	///<summary>
	///	Cette variable détermine le nombre de points de vie du building
	///</summary>
	[SerializeField]
	private int health;

	///<summary>
	///	Cette variable détermine le score accordé quand le building est coloré
	///</summary>
	[SerializeField]
	private int scoreValue;

	///<summary>
	///	Cette variable détermine si le building peut être touché ou non
	///</summary>
	private bool canBeHit = true;

	#endregion

	#region Functions

	private void Start()
	{
		colorEverythingGray();
	}

	///<summary>
	///	Cette fonction se déclenche lorsqu'un collider de type "trigger" entre en collision avec le building
	///</summary>
	private void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Projectile" && !col.gameObject.GetComponent<ProjectileManager>().isEnemy)
		{
			if(canBeHit)
			{
				modifyHealth(-col.gameObject.GetComponent<ProjectileManager>().getDamage(), col.gameObject.GetComponent<Renderer>().material.color);
			}
			AudioSource.PlayClipAtPoint(MainManager.INSTANCE.sonDeTirsSplat, col.transform.position);
			Destroy(col.gameObject);
		}
	}

	///<summary>
	///	Cette fonction nous permet de modifier la vie du building et de lancer la fonction de coloriage si sa vie descend assez bas
	///</summary>
	private void modifyHealth(int modifier, Color projectileColor)
	{
		health += modifier;
		if(health <= 0)
		{
			canBeHit = false;
			colorBuilding(projectileColor);
			Debug.Log("Adding " + scoreValue + " to Score !");
			MainManager.INSTANCE.scoreManager.addToScore(scoreValue);
			//Color the building
		}
		else
		{
			StartCoroutine(buildingFlash());
		}
	}

	///<summary>
	///	Cette fonction colorie le building et tous les batiments autour
	///</summary>
	private void colorBuilding(Color projectileColor)
	{
		//Si le building possède une tourelle, on détruit la tourelle
		if(gameObject.GetComponent<BuildingFireManager>() != null)
		{
			gameObject.GetComponent<BuildingFireManager>().explodeTurret();
		}

		//On colorie le building principal
		if(gameObject.GetComponent<Renderer>() != null)
		{
			foreach(Material mat in gameObject.GetComponent<Renderer>().materials)
			{
				mat.color = projectileColor;
			}
		}

		List<Color> randomColors = MainManager.INSTANCE.getRandomColors();

		//On colorie les buildings de décor
		foreach(Transform building in transform)
		{
			if(building.GetComponent<Renderer>() != null)
			{
				foreach(Material mat in building.GetComponent<Renderer>().materials)
				{
					mat.color = randomColors[Random.Range(0, randomColors.Count)];
				}
			}
		}

		gameObject.GetComponent<AudioSource>().clip = MainManager.INSTANCE.sonDeBuildingColore;
		gameObject.GetComponent<AudioSource>().Play();
	}

	///<summary>
	///	Cette fonction colorie le building et tous ceux autour en gris
	///</summary>
	private void colorEverythingGray()
	{
		//On colorie le building principal
		foreach(Material mat in gameObject.GetComponent<Renderer>().materials)
		{
			mat.color = Color.grey;
		}

		//On colorie les buildings de décor
		foreach(Transform building in transform)
		{
			if(building.GetComponent<Renderer>() != null)
			{
				foreach(Material mat in building.GetComponent<Renderer>().materials)
				{
					mat.color = Color.grey;
				}
			}
		}
	}

	///<summary>
	///	Cette fonction fait clignoter le building
	///</summary>
	private IEnumerator buildingFlash()
	{
		foreach(Material mat in gameObject.GetComponent<Renderer>().materials)
		{
			mat.color = Color.white;
		}

		yield return new WaitForSeconds(0.05f);

		colorEverythingGray();
	}

	public void setHealth(int newHealth)
	{
		health = newHealth;
	}

	public void setScore(int newScore)
	{
		scoreValue = newScore;
	}

	public bool isColored()
	{
		return health <= 0;
	}

	#endregion	
}
