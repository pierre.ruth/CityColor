﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeedbackManager : MonoBehaviour {

	private Transform camTransform;
	private Vector3 originalPos;

	[SerializeField]
	private float cameraShakeDuration;

	[SerializeField]
	private float shakeAmount;

	void Start () 
	{
		camTransform = Camera.main.transform;
		originalPos = camTransform.position;
	}

	[ContextMenu("Shake it !")]
	public void shakeCamera(float duration = -1f)
	{
		if(duration == -1f)
		{
			duration = cameraShakeDuration;
		}
		StartCoroutine(shakeCameraCoroutine(duration));
	}

	private IEnumerator shakeCameraCoroutine(float duration)
	{
		float countdown = duration;

		while(countdown > 0)
		{
			camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
			countdown -= Time.deltaTime;
			yield return null;
		}
	}
	
	
}
