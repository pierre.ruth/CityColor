/********************************************
* Product:	CityColor
* Developer:	Pierre Ruth - pierre.ruth@larelevedufutur.org
* Company:	La Relève Du Futur
* Date:		27/06/2018 19:41:00
********************************************/

using UnityEngine;

using TMPro;

public class ScoreManager : MonoBehaviour 
{
	#region Variables

	///<summary>
	///	Cette variable détermine le score du joueur
	///</summary>
	[SerializeField]
	private int score;

	///<summary>
	///	Cette variable contient le texte indiquant le score du joueur
	///</summary>
	[SerializeField]
	private TextMeshProUGUI scoreText;

	#endregion

	#region Functions

	private void Start()
	{
		addToScore(0);
	}

	public void addToScore(int value)
	{
		score += value;
		scoreText.text = "Score : " + score;
	}

	#endregion	
}
