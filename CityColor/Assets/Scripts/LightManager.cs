﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour {

	#region Variables

	public enum LightMode
	{
		DUSK,
		NOON,
		DAWN,
		TWILIGHT
	}

	///<summary>
	///	Cette variable contient l'objet générant la lumière (et les ombres)
	///</summary>
	private Light mainLight;

	///<summary>
	///	Cette variable détermine le type de lumière
	///</summary>
	[SerializeField]
	private LightMode lightMode;

	///<summary>
	///	Cette variable détermine la couleur du LightMode : DUSK (Matin)
	///</summary>
	[SerializeField]
	private Color duskColor = Color.white;

	///<summary>
	///	Cette variable détermine la couleur du LightMode : NOON (Midi)
	///</summary>
	[SerializeField]
	private Color noonColor = Color.white;

	///<summary>
	///	Cette variable détermine la couleur du LightMode : DAWN (Crépuscule)
	///</summary>
	[SerializeField]
	private Color dawnColor = Color.white;

	///<summary>
	///	Cette variable détermine la couleur du LightMode : TWILIGHT (Soirée)
	///</summary>
	[SerializeField]
	private Color twilightColor = Color.white;

	#endregion

	#region Functions

	///<summary>
	/// On initialise le manager
	///</summary>
	void Start () 
	{
		mainLight = gameObject.GetComponent<Light>();
		//setLight(lightMode);
	}
	
	///<summary>
	/// On met en place la lumière
	///</summary>
	public void setLight(LightMode newLight)
	{
		switch(newLight)
		{
			case LightMode.DUSK:
				transform.rotation = Quaternion.Euler(new Vector3(50f, -60f, 0f));
				mainLight.color = noonColor;
				mainLight.intensity = 0.75f;
				break;

			case LightMode.NOON:
				transform.rotation = Quaternion.Euler(new Vector3(50f, 0f, 0f));
				mainLight.color = noonColor;
				mainLight.intensity = 1f;
				break;

			case LightMode.DAWN:
				transform.rotation = Quaternion.Euler(new Vector3(50f, 60f, 0f));
				mainLight.color = dawnColor;
				mainLight.intensity = 1.25f;
				break;

			case LightMode.TWILIGHT:
				transform.rotation = Quaternion.Euler(new Vector3(50f, -30f, 0f));
				mainLight.color = twilightColor;
				mainLight.intensity = 1f;
				break;

			default:
				Debug.LogError("Not a valid LightMode !");
				break;
		}
	}

	#endregion
}
