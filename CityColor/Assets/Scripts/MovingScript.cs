﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingScript : MonoBehaviour {

	#region Variables

	///<summary>
	///	Cette variable détermine si la vitesse de l'objet se cale sur celle du niveau
	///</summary>
	[SerializeField]
	private bool isLevelSpeed = false;

	///<summary>
	///	Cette variable détermine la vitesse de l'objet
	///</summary>
	[SerializeField]
	private float speed;

	#endregion

	#region Functions

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(MainManager.INSTANCE.levelManager.getIsMoving())
		{
			if(isLevelSpeed)
			{
				transform.Translate(new Vector3(0f, 0f, -MainManager.INSTANCE.levelManager.getMovingSpeed()));
			}
			else
			{
				transform.Translate(new Vector3(0f, 0f, -speed));
			}
		}
	}

	#endregion
}
