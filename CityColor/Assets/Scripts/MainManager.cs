/********************************************
* Product:	CityColor
* Developer:	Pierre Ruth - pierre.ruth@larelevedufutur.org
* Company:	La Relève Du Futur
* Date:		27/06/2018 13:22:03
********************************************/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class MainManager : MonoBehaviour 
{
	#region Singleton
	private static MainManager instance;
	private MainManager(){}
	public static MainManager INSTANCE
	{
		get
		{
			if(instance == null)
			{
				instance = GameObject.FindObjectOfType<MainManager>();
				if(instance == null)
				{
					Debug.LogError("IMPOSSIBLE DE TROUVER L'OBJET DE TYPE MAINMANAGER !");
				}
			}

			return instance;
		}
	}
	#endregion

	#region Variables

	[Header("PAS TOUCHE A CES VARIABLES !")]
	public PlayerManager playerManager;
	public ScoreManager scoreManager;
	public LevelManager levelManager;
	public PauseManager pauseManager;
	public FeedbackManager feedbackManager;
	public LightManager lightManager;

	[SerializeField]
	private GameObject rain;

	[Header("Variables de Couleur")]
	///<summary>
	///	Cette variable contient les couleurs qui pourront être associées à un material des batiments de décor
	///</summary>
	[SerializeField]
	private List<Color> couleursDeTir = new List<Color>();

	[Header("Variables de Batiments")]
	///<summary>
	///	Cette variable contient le nombre de points de vie des batiments normaux
	///</summary>
	[SerializeField]
	private int batimentPointDeVie;

	///<summary>
	///	Cette variable contient le score que donnent les batiments normaux
	///</summary>
	[SerializeField]
	private int batimentScore;

	///<summary>
	///	Cette variable contient le nombre de points de vie des batiments qui tirent
	///</summary>
	[SerializeField]
	private int batimentTirPointDeVie;

	///<summary>
	///	Cette variable contient le score que donnent les batiments qui tirent
	///</summary>
	[SerializeField]
	private int batimentTirScore;

	[Header("Variables de Parcs")]
	///<summary>
	///	Cette variable contient le score que donnent les parcs
	///</summary>
	[SerializeField]
	private int parcScore;

	[Header("Variables de Vaisseau")]
	///<summary>
	///	Cette variable contient le nombre de vies du joueur
	///</summary>
	[SerializeField]
	private int viesDuJoueur;

	///<summary>
	///	Cette variable contient la vitesse du joueur
	///</summary>
	[SerializeField]
	private float vitesseDuJoueur;

	///<summary>
	///	Cette variable contient la rotation du joueur quand il va vers la droite/gauche
	///</summary>
	[SerializeField]
	private float rotationDuJoueur;

	[Header("Variables de Niveau")]
	///<summary>
	///	Cette variable contient la vitesse de défilement du niveau
	///</summary>
	[SerializeField]
	private float vitesseDuNiveau;

	[Header("Variables d'Effets")]
	///<summary>
	///	Cette variable détermine la lumière du niveau
	///</summary>
	[SerializeField]
	private LightManager.LightMode lumiereDuNiveau;

	///<summary>
	///	Cette variable détermine si on active la pluie ou non
	///</summary>
	[SerializeField]
	private bool pluiePresente = false;

	[Header("Variables d'Icones")]
	///<summary>
	///	Cette variable détermine l'icône des vies
	///</summary>
	[SerializeField]
	private Sprite iconeDeVie;

	///<summary>
	///	Cette variable détermine l'icône des boutons
	///</summary>
	[SerializeField]
	private Sprite iconeDeBouton;

	[Header("Variables de Musique")]
	///<summary>
	///	Cette variable détermine la musique du niveau
	///</summary>
	[SerializeField]
	private AudioClip musiqueDuNiveau;

	///<summary>
	///	Cette variable détermine le volume de la musique du niveau
	///</summary>
	[SerializeField]
	private float volumeDeMusique = 1f;

	[Header("Variables de Sons")]
	///<summary>
	///	Cette variable détermine le son des explosions
	///</summary>
	public AudioClip sonDExplosion;

	///<summary>
	///	Cette variable détermine le son des tirs de vaisseau
	///</summary>
	public AudioClip sonDeTirs;

	///<summary>
	///	Cette variable détermine le son des tirs de vaisseau lorsqu'ils touchent un building
	///</summary>
	public AudioClip sonDeTirsSplat;

	///<summary>
	///	Cette variable détermine le son du vaisseau
	///</summary>
	public AudioClip sonDeVaisseau;

	///<summary>
	///	Cette variable détermine le son de larguage de bombe fleur
	///</summary>
	public AudioClip sonDeBombeFleurLarguee;

	///<summary>
	///	Cette variable détermine le son d'explosion de bombe fleur
	///</summary>
	public AudioClip sonDeBombeFleurExplosion;

	///<summary>
	///	Cette variable détermine le son lorsqu'un parc fleurit
	///</summary>
	public AudioClip sonDeParcFleuri;

	///<summary>
	///	Cette variable détermine le son lorsqu'un building est coloré
	///</summary>
	public AudioClip sonDeBuildingColore;

	///<summary>
	///	Cette variable détermine le son d'un bouton
	///</summary>
	public AudioClip sonDeBouton;

	///<summary>
	///	Cette variable détermine le son du compte à rebours
	///</summary>
	public AudioClip sonDeCompteARebours;

	///<summary>
	///	Cette variable détermine le son indiquant le début du jeu
	///</summary>
	public AudioClip sonDeDepart;

	#endregion

	#region Functions

	private void Start()
	{
		if(batimentPointDeVie != 0 && batimentScore != 0)
		{
			levelManager.setBuildingParameters("normal", batimentPointDeVie, batimentScore);
		}

		if(batimentTirPointDeVie != 0 && batimentTirScore != 0)
		{
			levelManager.setBuildingParameters("firing", batimentTirPointDeVie, batimentTirScore);
		}

		if(parcScore != 0)
		{
			levelManager.setParkParameters(parcScore);
		}

		if(viesDuJoueur != 0 && vitesseDuJoueur != 0 && rotationDuJoueur != 0)
		{
			playerManager.setPlayerValues(viesDuJoueur, vitesseDuJoueur, rotationDuJoueur);
		}

		if(vitesseDuNiveau != 0)
		{
			levelManager.setMovingSpeed(vitesseDuNiveau);
		}

		lightManager.setLight(lumiereDuNiveau);

		if(pluiePresente)
		{
			rain.SetActive(true);
		}
		else
		{
			rain.SetActive(false);
		}

		Camera.main.GetComponent<AudioSource>().clip = musiqueDuNiveau;
		Camera.main.GetComponent<AudioSource>().volume = volumeDeMusique;
		Camera.main.GetComponent<AudioSource>().Play();

		playerManager.setLifeSprite(iconeDeVie);
		setButtonIcon();
	}

	public List<Color> getRandomColors()
	{
		return couleursDeTir;
	}

	private void setButtonIcon()
	{
		GameObject[] listOfMenus = GameObject.FindGameObjectsWithTag("Menu");
		Debug.Log(listOfMenus.Length);
		foreach(GameObject menu in listOfMenus)
		{
			foreach(Transform element in menu.transform)
			{
				if(element.GetComponent<Button>() != null)
				{
					element.GetComponent<Image>().sprite = iconeDeBouton;
				}
			}
		}
	}

	#endregion
}
