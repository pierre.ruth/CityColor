﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectsMover : MonoBehaviour {

	#region Variables

	///<summary>
	///	Cette variable contient le transform du niveau
	///</summary>
	private Transform level;

	///<summary>
	///	Cette variable contient la position initiale du niveau
	///</summary>
	private Vector3 levelPos;

	#endregion

	#region Functions

	// Use this for initialization
	void Start () 
	{
		level = MainManager.INSTANCE.levelManager.transform;
		levelPos = level.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if(level.position.z < levelPos.z && levelPos.z - level.position.z >= 4f)
		{
			//Debug.Log("Moving effects forward");
			levelPos = level.position;
			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + 4f);
		}
		else if(level.position.z > levelPos.z && level.position.z - levelPos.z >= 4f)
		{
			//Debug.Log("Moving effects backwards");
			levelPos = level.position;
			transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z - 4f);
		}
	}

	#endregion
}
