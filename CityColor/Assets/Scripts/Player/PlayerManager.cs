﻿/********************************************
* Product:	#PROJECTNAME#
* Developer:	#DEVELOPERNAME#
* Company:	#COMPANY#
* Date:		#CREATIONDATE#
********************************************/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour 
{
	#region Variables

	[System.Serializable]
	public class Boundaries
	{
		public float xMin;
		public float xMax;
		public float zMin;
		public float zMax;
	}

	[Header("Variables de Vie")]
	///<summary>
	///	Cette variable indique le nombre de vies restantes au joueur
	///</summary>
	[SerializeField]
	private int lives;

	///<summary>
	///	Cette variable contient l'endroit où les vies sont placées sur l'UI
	///</summary>
	[SerializeField]
	private Transform livesContentUI;

	///<summary>
	///	Cette variable contient le gameObject réprésentant les vies du joueur
	///</summary>
	[SerializeField]
	private GameObject lifeUI;
	
	private Sprite lifeSprite;

	///<summary>
	///	Cette variable contient le gameObject explosion
	///</summary>
	[SerializeField]
	private GameObject explosion;

	private Renderer rend;

	private bool isDead = false;

	[Header("Variables de Mouvement")]
	///<summary>
	///	Cette variable gère la vitesse du vaisseau
	///</summary>
	[SerializeField]
	private float speed;

	///<summary>
	///	Cette variable gère l'angle de rotation du vaisseau (quand on va vers la droite ou la gauche)
	///</summary>
	[SerializeField]
	private float tilt;

	///<summary>
	///	Cette variable gère les limites de déplacement du vaisseau
	///</summary>
	[SerializeField]
	private Boundaries boundaries;

	///<summary>
	///	Cette variable contient le rigidbody du vaisseau
	///</summary>
	private Rigidbody rb;

	///<summary>
	///	Cette variable détermine si le vaisseau peut se déplacer
	///</summary>
	public bool canMove = true;

	[Header("Variables de Tir")]
	///<summary>
	///	Cette variable indique où se situe l'endroi d'où les projectiles sortiront
	///</summary>
	[SerializeField]
	private Transform cannonMouth;

	///<summary>
	///	Cette variable indique où se situe l'endroit d'où les bombes fleurs sortiront
	///</summary>
	[SerializeField]
	private Transform bombHatch;

	///<summary>
	///	Cette variable contient les projectiles du joueur
	///</summary>
	[SerializeField]
	private GameObject playerProjectiles;
	
	///<summary>
	///	Cette variable contient les bombes fleurs du joueur
	///</summary>
	[SerializeField]
	private GameObject flowerBombs;

	///<summary>
	///	Cette variable détermine le temps d'attente entre deux tirs
	///</summary>
	[SerializeField]
	private float projCooldown;

	///<summary>
	///	Cette variable détermine le temps d'attente entre deux bombes fleurs
	///</summary>
	[SerializeField]
	private float bombCooldown;

	///<summary>
	///	Cette variable détermine la couleur des projectiles
	///</summary>
	[SerializeField]
	private List<Color> projectileColors;

	///<summary>
	///	Cette variable, lorsqu'elle est "TRUE" permet au vaisseau de tirer
	///</summary>
	private bool canFire = true;

	///<summary>
	///	Cette variable, lorsqu'elle est "TRUE" permet au vaisseau de larguer des bombes fleurs
	///</summary>
	private bool canFireBomb = true;

	[SerializeField]
	private GameObject audioShoot;

	///<summary>
	///	Cette variable détermine le son du vaisseau
	///</summary>
	private AudioClip sonDeVaisseau;

	#endregion

	#region Functions

	///<summary>
	///	Cette fonction est lancée dès que le jeu démarre
	///</summary>
	private void Start()
	{
		rb = GetComponent<Rigidbody>();
		rend = GetComponent<Renderer>();
		audioShoot.GetComponent<AudioSource>().clip = MainManager.INSTANCE.sonDeTirs;
		sonDeVaisseau = MainManager.INSTANCE.sonDeVaisseau;
		gameObject.GetComponent<AudioSource>().clip = sonDeVaisseau;
		gameObject.GetComponent<AudioSource>().Play();
		for(int i = 0; i < lives; i++)
		{
			GameObject newLife = Instantiate(lifeUI, livesContentUI);
			newLife.GetComponent<Image>().sprite = lifeSprite;
		}
	}

	///<summary>
	///	Cette fonction est lancée à chaque frame (60 fps = 60 frames par seconde)
	///</summary>
	private void FixedUpdate()
	{
		if(canMove)
		{
			float moveHorizontal = Input.GetAxis ("Horizontal");
	        float moveVertical = Input.GetAxis ("Vertical");

	        Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
	        rb.velocity = movement * speed;

			if(moveHorizontal == 0f)
			{
				gameObject.GetComponent<AudioSource>().volume = 0.5f;
			}
			else
			{
				gameObject.GetComponent<AudioSource>().volume = 1f;
			}

	        rb.position = new Vector3 
	        (
	            Mathf.Clamp (rb.position.x, boundaries.xMin, boundaries.xMax), 
	            0.0f, 
	            Mathf.Clamp (rb.position.z, boundaries.zMin, boundaries.zMax)
	        );

	        rb.rotation = Quaternion.Lerp(rb.rotation, Quaternion.Euler (0.0f, 0.0f, rb.velocity.x * -tilt), 0.3f);

	        if(Input.GetButton("Fire1") && canFire)
	        {
	        	GameObject projectile = Instantiate(playerProjectiles, cannonMouth.position, Quaternion.identity, transform.parent);
	        	projectile.GetComponent<Renderer>().material.SetColor("_Color", projectileColors[Random.Range(0, projectileColors.Count)]);
				audioShoot.GetComponent<AudioSource>().Play();
	        	StartCoroutine(projectileCooldown());
	        }

			if(Input.GetButton("Jump") && canFireBomb)
	        {
				Instantiate(flowerBombs, bombHatch.position, Quaternion.identity, transform.parent);
	        	StartCoroutine(bombsCooldown());
	        }
	    }
	}

	///<summary>
	///	Cette fonction permet d'empêcher au joueur d'envoyer trop de projectiles à la fois
	///</summary>
	private IEnumerator projectileCooldown()
	{
		canFire = false;
		yield return new WaitForSeconds(projCooldown);
		canFire = true;
	}

	///<summary>
	///	Cette fonction permet d'empêcher au joueur d'envoyer trop de projectiles à la fois
	///</summary>
	private IEnumerator bombsCooldown()
	{
		canFireBomb = false;
		yield return new WaitForSeconds(bombCooldown);
		canFireBomb = true;
	}

	///<summary>
	///	Cette fonction est lancée à chaque fois que le joueur entre en collision avec quelque chose
	///</summary>
	private void OnCollisionEnter(Collision col)
	{
		if(!isDead)
		{
			StartCoroutine(playerDiesCoroutine());
		}
	}

	///<summary>
	///	Cette fonction est lancée à chaque fois que le joueur entre en collision avec quelque chose qui a un collider en trigger
	///</summary>
	private void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Projectile" && !isDead)
		{
			if(col.GetComponent<ProjectileManager>().isEnemy)
			{
				Destroy(col.gameObject);
				StartCoroutine(playerDiesCoroutine());
			}
		}	
	}

	///<summary>
	///	Cette fonction est lancée lorsque le joueur perd une vie
	///</summary>
	private IEnumerator playerDiesCoroutine()
	{
		isDead = true;
		canMove = false;
		rend.enabled = false;
		lives--;

		updateLifeCount();
		MainManager.INSTANCE.feedbackManager.shakeCamera(0.5f);

		StartCoroutine(playerExplodesCoroutine());

		if(lives <= 0)
		{
			//GAME OVER
		}
		else
		{
			yield return new WaitForSeconds(1f);
			transform.position = new Vector3(0, 0, 0);
			canMove = true;
			for(int i = 0; i < 4; i++)
			{
				rend.enabled = true;
				yield return new WaitForSeconds(0.5f);
				rend.enabled = false;
				yield return new WaitForSeconds(0.5f);
			}
			rend.enabled = true;
			yield return new WaitForSeconds(0.5f);
		}

		isDead = false;
	}

	///<summary>
	///	Cette fonction est lancée pour faire exploser le vaisseau
	///</summary>
	private IEnumerator playerExplodesCoroutine()
	{
		Vector3 explosionPos = transform.parent.InverseTransformPoint(transform.position);
		GameObject newExplosion = Instantiate(explosion, explosionPos, Quaternion.identity, transform.parent);
		yield return new WaitForSeconds(2f);
		Destroy(newExplosion);
	}

	///<summary>
	///	Cette fonction est utilisée pour mettre à jour le nombre de vies affichées
	///</summary>
	private void updateLifeCount()
	{
		int livesCount = lives;

		foreach(Transform element in livesContentUI)
		{
			if(livesCount <= 0)
			{
				Destroy(element.gameObject);
			}
			livesCount--;
		}
	}

	public void playerEndLevel()
	{
		canMove = false;
		StartCoroutine(playerEndLevelCoroutine());
	}

	private IEnumerator playerEndLevelCoroutine()
	{
		Vector3 movement = new Vector3 (0f, 0.0f, 1f);
	    rb.velocity = movement * speed;
	    yield return new WaitForSeconds(4f);
	    rb.velocity = Vector3.zero;
	}

	public void setPlayerValues(int newLives, float newSpeed, float newTilt)
	{
		lives = newLives;
		speed = newSpeed;
		tilt = newTilt;
	}

	public void setLifeSprite(Sprite newLifeSprite)
	{
		lifeSprite = newLifeSprite;
	}

	#endregion
}
