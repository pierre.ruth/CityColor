﻿/********************************************
* Product:	#PROJECTNAME#
* Developer:	#DEVELOPERNAME#
* Company:	#COMPANY#
* Date:		#CREATIONDATE#
********************************************/

using UnityEngine;
using System.Collections;

public class ProjectileManager : MonoBehaviour 
{
	#region Variables

	///<summary>
	///	Cette variable détermine la vitesse du projectile
	///</summary>
	[SerializeField]
	private float speed;

	///<summary>
	///	Cette variable contient le rigidbody du projectile
	///</summary>
	[SerializeField]
	private Rigidbody rb;

	///<summary>
	///	Cette variable détermine à quel moment le projectile disparaît
	///</summary>
	[SerializeField]
	private float zLimit;

	///<summary>
	///	Cette variable détermine après combien de temps le projectile disparaît après avoir disparu de l'écran
	///</summary>
	[SerializeField]
	private float timeBeforeDestroy;

	///<summary>
	///	Cette variable détermine les dommages du projectile
	///</summary>
	[SerializeField]
	private int damage;

	///<summary>
	///	Cette variable détermine si le projectile vient d'un ennemi ou non
	///</summary>
	public bool isEnemy;

	#endregion

	#region Functions

	private void Start()
	{
		if(isEnemy)
		{
			transform.LookAt(MainManager.INSTANCE.playerManager.transform);
		}
	}


	private void FixedUpdate()
	{
		if(isEnemy)
		{
			rb.velocity = (transform.rotation /** Quaternion.Euler(0f, -90f, 0f)*/ * Vector3.forward) * speed;//new Vector3 (0f, 0.0f, speed);
		}
		else
		{
			rb.velocity = new Vector3 (0f, 0.0f, speed);
		}

		if(transform.position.z > zLimit || isEnemy)
		{
			StartCoroutine(destroyProjectile());
		}
	}

	private IEnumerator destroyProjectile()
	{
		yield return new WaitForSeconds(timeBeforeDestroy);
		Destroy(gameObject);
	}

	public int getDamage()
	{
		return damage;
	}

	#endregion	
}
