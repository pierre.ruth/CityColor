using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerBombManager : MonoBehaviour {

	[SerializeField]
	private float speed;

	[SerializeField]
	private Vector3 finalScale;

	[SerializeField]
	private float rotationSpeed;

	private bool isFalling = true;

	private Rigidbody rb;

	private AudioSource audioSource;

	private void Start()
	{
		rb = gameObject.GetComponent<Rigidbody>();
		audioSource = gameObject.GetComponent<AudioSource>();
		audioSource.clip = MainManager.INSTANCE.sonDeBombeFleurLarguee;
		audioSource.Play();
	}

	private void FixedUpdate()
	{
		if(isFalling)
		{
			rb.velocity = new Vector3(0f, -speed, 0f);
			transform.rotation *= Quaternion.Euler(new Vector3(0f, rotationSpeed, 0f));
		}
	}

	private void OnTriggerEnter(Collider col)
	{
		if((col.gameObject.tag != "Projectile" && col.gameObject.tag != "Player") && isFalling)
		{
			StartCoroutine(flowerBloomCoroutine());
		}
	}

	private IEnumerator flowerBloomCoroutine()
	{
		isFalling = false;
		transform.parent = MainManager.INSTANCE.levelManager.transform;
		rb.velocity = Vector3.zero;

		audioSource.clip = MainManager.INSTANCE.sonDeBombeFleurExplosion;
		audioSource.Play();

		while(transform.localScale.x < finalScale.x)
		{
			transform.localScale = Vector3.Lerp(transform.localScale, finalScale * 1.05f, 0.5f);
			yield return null;
		}

		while(transform.localScale.x > 0.5f)
		{
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(0f, 0f, 0f), 0.3f);
			yield return null;
		}

		yield return new WaitForSeconds(1f);

		Destroy(gameObject);
	}
}
